﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeScript : MonoBehaviour
{
    public Text timeText = null;
    void Update()
    {
        timeText.text = Time.timeSinceLevelLoad + "s";
    }
}
