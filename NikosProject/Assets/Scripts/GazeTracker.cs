﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeTracker : MonoBehaviour
{
    public Camera playerCam;
    public Text ui;

    bool isInBox = false;
    float time = 0f;
    void Start()
    {

    }
    void Update()
    {
        if(!isInBox) return;

        int layerMask = 1 << 10;
        layerMask = ~layerMask;

        Vector3 pos = playerCam.transform.position;

        if(Physics.Raycast(pos, playerCam.transform.forward, out RaycastHit hit, float.MaxValue, layerMask)) {
            if(hit.collider.CompareTag("Mirror")) {
                time += Time.deltaTime;
            }
        }
    }

    private void FixedUpdate() {
        ui.text = time + " s";    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CamField"))
            isInBox = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("CamField"))
            isInBox = false;
    }
}
